package pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Sleeper;
import org.openqa.selenium.support.ui.WebDriverWait;

import testbase.Keywordlaunch;

public class Diy_Login_Page extends Keywordlaunch {

	@FindBy(xpath = "//span[text()='Register']")
	WebElement Register_path;

	@FindBy(xpath = "//input[@name='email']")
	WebElement MailId;

	@FindBy(xpath = "//span[text()='Continue']")
	WebElement Continue;

	@FindBy(xpath = "//input[@name='password']")
	WebElement Password;

	@FindBy(xpath = "//i[@class='_601b6d43 _0fa8876b b624439c _41e35d11']")
	WebElement Checkboxbq;

	@FindBy(xpath = "//span[text()='No thanks']")
	WebElement Radiobutton_Marketing;

	@FindBy(xpath = "//span[text()='Register' and @class='e433fa60 f9f4120c']")
	WebElement ClickRegister;

	public Diy_Login_Page() {
		PageFactory.initElements(driver, this);
	}

	public void Register_Diy() {
		WebDriverWait regclick=new WebDriverWait(driver,2000);
		regclick.until(ExpectedConditions.elementToBeClickable(Register_path));
		Register_path.click();
		MailId.clear();
		MailId.sendKeys("arundarll007@gmail.com");
		Continue.click();
		Password.sendKeys("Arundarll007@");
		Thread.sleep
		if (Checkboxbq.isSelected()) {
			try {
				driver.wait(20);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		else {
			Checkboxbq.click();
		}
                 Thread.sleep(200);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(Radiobutton_Marketing));

		ClickRegister.click();
	}

}
